<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false" %>
<%@ page import="java.time.format.DateTimeFormatter" %>

<html>
<head>

    <title> Users </title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/custom/css/table.css" />

</head>

<body>
    <header class="p-3 bg-dark text-white">
        <div class="container">
            <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
                <a href="/" class="d-flex align-items-center mb-2 mb-lg-0 text-white text-decoration-none">
                    <svg class="bi me-2" width="40" height="32" role="img" aria-label="Bootstrap">
                        <use xlink:href="#bootstrap"></use>
                    </svg>
                </a>

                <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
                    <li><a href="${pageContext.request.contextPath}/web/users" class="nav-link px-2 text-white">Users</a></li>
                    <li><a href="#" class="nav-link px-2 text-white">Contacts</a></li>
                </ul>

                <div class="text-end">
                    <button type="button" class="btn btn-outline-light me-2">Login</button>
                    <button type="button" class="btn btn-warning">Sign-up</button>
                </div>
            </div>
        </div>
    </header>
    <div id="table_div">
        <table class="table table-dark table-striped">
            <thead>
                <tr>
                    <th> ID </th>
                    <th> Login </th>
                    <th> Date of Birth </th>
                    <th> Date of registration </th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${users}" var="user">
                    <tr>
                        <td>${user.id}</td>
                        <td>${user.login}</td>
                        <td>${user.dateBorn}</td>
                        <td>${user.dateRegistration.format(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss.SSS"))}</td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>

    <div id="div_message">
        <c:out value="${message}"/>
    </div>

    <div id="useradd_div">
        <form action="<c:url value = "/web/useradd"/>" method="post">
            <input id="login_text" type="text" name="login" placeholder="login">
            <input id="password_text" type="text" name="password" placeholder="password">
            <input id="date_Born_date" type="date" name="dateBorn">
            <input id="add_user_submit" type="submit" value="  Add user  ">
        </form>
    </div>

    <div id="userdel_div">
        <form action="<c:url value = "/web/userdel"/>" method="post">
            <input id="del_text" type="text" name="login" placeholder="login">
            <input id="del_user_submit" type="submit" value=" Delete user">
        </form>
    </div>

</body>
</html>