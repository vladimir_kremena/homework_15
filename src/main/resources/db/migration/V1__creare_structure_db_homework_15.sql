CREATE TABLE users (
    id                  SERIAL8 PRIMARY KEY,
    login               VARCHAR(32) UNIQUE NOT NULL,
    password            VARCHAR(32) NOT NULL,
    date_born           DATE NOT NULL,
    date_registration   TIMESTAMP NOT NULL DEFAULT now()
);

insert into users(login, password, date_born)
values ('user_01', '9e4679a1c821a31b19f633553bfebd2',   '1970-01-01'),
       ('user_02', 'd2954386928c499219bbb35fdeffa5',    '1970-01-02'),
       ('user_03', '6e31fdd47743bca30cecca27a941c4d',   '1970-01-03'),
       ('user_04', '98de842cd10b15045f5893dfe10c838',   '1970-01-04'),
       ('user_05', 'b03598e0dcfbe05ee8dc8a7a19d87eb',   '1970-01-05'),
       ('user_06', '8fdb484695b69311d34a68a3e2f05bd0',  '1970-01-06'),
       ('user_07', '705edf1b92630ab3bba68ecddceee',     '1970-01-07'),
       ('user_08', '7f467ca8d776aa472bab33cfac5922e',   '1970-01-08'),
       ('user_09', 'f4c61c558e558b2a2720b4fa4a4a798a',  '1970-01-09'),
       ('user_10', '143992f0e4bb5c632a67904c9a9bcf',    '1970-01-10'),
       ('user_11', 'b7843d8a594c9d4af5174a942c45339',   '1970-01-11'),
       ('user_12', 'afdab7ace8325b2fa4c73573020a1a',    '1970-01-12'),
       ('user_13', '684190abff94c96d2ecfb4734536a34c',  '1970-01-13'),
       ('user_14', '7f6ba2374190223ae6eb3445699291aa',  '1970-01-14'),
       ('user_15', 'ce11f5ec5fc3352520dd6de4bec1d046',  '1970-01-15'),
       ('user_16', 'b29ed66f2820bdc7e2412169112611',    '1970-01-16'),
       ('user_17', '27766212754d574cfb952d641d8b36f6',  '1970-01-17'),
       ('user_18', 'c98ffe8ea8f768e0f99ef3b2e8986fa',   '1970-01-18'),
       ('user_19', '859751055df1cd34b8925ff601a26b2',   '1970-01-19'),
       ('user_20', '1684ff6399e2d71ea3328e156ee1c82',   '1970-01-20');