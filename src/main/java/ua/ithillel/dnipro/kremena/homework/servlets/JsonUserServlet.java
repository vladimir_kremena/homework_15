package ua.ithillel.dnipro.kremena.homework.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import ua.ithillel.dnipro.kremena.homework.dto.UserAddResponse;
import ua.ithillel.dnipro.kremena.homework.dto.UserDelResponse;
import ua.ithillel.dnipro.kremena.homework.dto.UsersResponse;
import ua.ithillel.dnipro.kremena.homework.dto.enums.ResponseStatus;
import ua.ithillel.dnipro.kremena.homework.factory.UserServiceFactory;
import ua.ithillel.dnipro.kremena.homework.factory.impl.DatabaseUserServiceFactory;
import ua.ithillel.dnipro.kremena.homework.model.User;
import ua.ithillel.dnipro.kremena.homework.service.UserService;
import ua.ithillel.dnipro.kremena.homework.utils.properties.AppProperties;
import ua.ithillel.dnipro.kremena.homework.utils.validator.Validator;
import ua.ithillel.dnipro.kremena.homework.utils.validator.impl.UserValidator;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@WebServlet(urlPatterns = {"/json/users", "/json/useradd", "/json/userdel"})
public class JsonUserServlet extends HttpServlet {

    private final UserServiceFactory userServiceFactory =
            createUserServiceFactory(AppProperties.INSTANCE.getProperties().getProperty("application.mode"));
    private final UserService userService = userServiceFactory.createUserService();
    private final ObjectMapper objectMapper = new ObjectMapper().registerModule(new JavaTimeModule())
            .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    private final Validator validator = new UserValidator();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getServletPath().equals("/json/users")) {
            resp.setHeader("Content-type", "application/json");
            List<User> users = userService.getAll();
            if (users.size() != 0) {
                resp.getOutputStream().write(
                        objectMapper.writeValueAsBytes(
                                UsersResponse.builder()
                                        .responseStatus(ResponseStatus.OK)
                                        .users(users)
                                        .build()
                        )
                );
            } else {
                resp.getOutputStream().write(
                        objectMapper.writeValueAsBytes(
                                UsersResponse.builder()
                                        .responseStatus(ResponseStatus.ERROR)
                                        .error("User list is empty")
                                        .build()
                        )
                );
            }
            resp.getOutputStream().flush();
        } else {
            super.doGet(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getServletPath().equals("/json/useradd")) {
            resp.setHeader("Content-type", "application/json");
            User incomingUser = objectMapper.readValue(req.getInputStream().readAllBytes(), User.class);
            if (validator.isValid(incomingUser)) {
                User databaseUser = userService.getByLogin(incomingUser.getLogin());
                if (incomingUser.getLogin().equals(databaseUser.getLogin())) {
                    resp.getOutputStream().write(objectMapper.writeValueAsBytes(
                            UserAddResponse.builder()
                                    .responseStatus(ResponseStatus.ERROR)
                                    .error("Login is already taken")
                                    .build()
                            )
                    );
                } else {
                    if (userService.add(incomingUser)) {
                        resp.getOutputStream().write(objectMapper.writeValueAsBytes(
                                UserAddResponse.builder()
                                        .responseStatus(ResponseStatus.OK)
                                        .build()
                                )
                        );
                    }
                }
            } else {
                resp.getOutputStream().write(objectMapper.writeValueAsBytes(
                        UserAddResponse.builder()
                                .responseStatus(ResponseStatus.ERROR)
                                .error(validator.fullnessCheck(incomingUser))
                                .build()
                        )
                );
            }
            resp.getOutputStream().flush();
        } else {
            super.doPost(req, resp);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getServletPath().equals("/json/userdel")) {
            resp.setHeader("Content-type", "application/json");
            User incomingUser = objectMapper.readValue(req.getInputStream().readAllBytes(), User.class);
            if (userService.delete(incomingUser)) {
                resp.getOutputStream().write(objectMapper.writeValueAsBytes(
                        UserDelResponse.builder()
                                .responseStatus(ResponseStatus.OK)
                                .build()
                        )
                );
            } else {
                resp.getOutputStream().write(objectMapper.writeValueAsBytes(
                        UserDelResponse.builder()
                                .responseStatus(ResponseStatus.ERROR)
                                .error("User is not found")
                                .build()
                        )
                );
            }
            resp.getOutputStream().flush();
        } else {
            super.doDelete(req, resp);
        }
    }

    private UserServiceFactory createUserServiceFactory(String applicationMode) {
        Map<String, UserServiceFactory> factories = Map.of(
                "DB", new DatabaseUserServiceFactory()
        );
        return factories.get(applicationMode);
    }
}