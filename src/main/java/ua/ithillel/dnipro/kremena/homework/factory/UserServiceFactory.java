package ua.ithillel.dnipro.kremena.homework.factory;

import ua.ithillel.dnipro.kremena.homework.service.UserService;

public interface UserServiceFactory {

    UserService createUserService();

}