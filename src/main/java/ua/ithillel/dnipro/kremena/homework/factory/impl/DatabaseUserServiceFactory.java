package ua.ithillel.dnipro.kremena.homework.factory.impl;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ua.ithillel.dnipro.kremena.homework.factory.UserServiceFactory;
import ua.ithillel.dnipro.kremena.homework.jdbc.JdbcTemplate;
import ua.ithillel.dnipro.kremena.homework.service.UserService;
import ua.ithillel.dnipro.kremena.homework.service.impl.DatabaseUserService;
import ua.ithillel.dnipro.kremena.homework.utils.properties.AppProperties;

import javax.sql.DataSource;

public class DatabaseUserServiceFactory implements UserServiceFactory {

    @Override
    public UserService createUserService() {
        return new DatabaseUserService(createJdbcTemplate());
    }

    private JdbcTemplate createJdbcTemplate() {
        return new JdbcTemplate(createDatasource());
    }

    private DataSource createDatasource() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName(AppProperties.INSTANCE.getProperties().getProperty("driver.class.name"));
        hikariConfig.setJdbcUrl(AppProperties.INSTANCE.getProperties().getProperty("database.uri"));
        hikariConfig.setUsername(AppProperties.INSTANCE.getProperties().getProperty("database.user"));
        hikariConfig.setPassword(AppProperties.INSTANCE.getProperties().getProperty("database.password"));
        hikariConfig.setMinimumIdle(
                Integer.parseInt(AppProperties.INSTANCE.getProperties().getProperty("minimum.idle")));
        hikariConfig.setMaximumPoolSize(
                Integer.parseInt(AppProperties.INSTANCE.getProperties().getProperty("maximum.pool.size")));
        return new HikariDataSource(hikariConfig);
    }
}