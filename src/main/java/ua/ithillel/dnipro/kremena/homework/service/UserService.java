package ua.ithillel.dnipro.kremena.homework.service;

import ua.ithillel.dnipro.kremena.homework.model.User;

import java.util.List;

public interface UserService {

    List<User> getAll();

    boolean add(User user);

    boolean delete(User user);

    User getByLogin(String login);

}