package ua.ithillel.dnipro.kremena.homework.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ua.ithillel.dnipro.kremena.homework.dto.enums.ResponseStatus;

@Getter @Setter
@Accessors(chain = true)
@Builder(toBuilder = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class UserAddResponse {
    @JsonProperty("status")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ResponseStatus responseStatus;
    private String error;
}