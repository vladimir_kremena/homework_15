package ua.ithillel.dnipro.kremena.homework.servlets;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import ua.ithillel.dnipro.kremena.homework.factory.UserServiceFactory;
import ua.ithillel.dnipro.kremena.homework.factory.impl.DatabaseUserServiceFactory;
import ua.ithillel.dnipro.kremena.homework.model.User;
import ua.ithillel.dnipro.kremena.homework.service.UserService;
import ua.ithillel.dnipro.kremena.homework.utils.properties.AppProperties;
import ua.ithillel.dnipro.kremena.homework.utils.validator.Validator;
import ua.ithillel.dnipro.kremena.homework.utils.validator.impl.UserValidator;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Map;

@WebServlet(urlPatterns = {"/web/users", "/web/useradd", "/web/userdel"})
public class WebUserServlet extends HttpServlet {

    private final UserServiceFactory userServiceFactory =
            createUserServiceFactory(AppProperties.INSTANCE.getProperties().getProperty("application.mode"));
    private final UserService userService = userServiceFactory.createUserService();
    private final Validator validator = new UserValidator();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(req.getServletPath().equals("/web/users")) {
            if (req.getParameter("message") != null) {
                req.setAttribute("message", req.getParameter("message"));
            }
            req.setAttribute("users", userService.getAll());
            getServletContext().getRequestDispatcher("/WEB-INF/views/users.jsp").forward(req, resp);
        } else {
            super.doGet(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User addUser = new User();
        switch (req.getServletPath()) {
            case "/web/useradd":
                try {
                    addUser.setLogin(req.getParameter("login"));
                    addUser.setPassword(req.getParameter("password"));
                    addUser.setDateBorn(LocalDate.parse(req.getParameter("dateBorn"),
                            DateTimeFormatter.ofPattern("yyyy-MM-dd")));
                } catch (DateTimeParseException dTPE) {
                    addUser.setLogin(req.getParameter("login"));
                    addUser.setPassword(req.getParameter("password"));
                    addUser.setDateBorn(LocalDate.of(1970, 1, 1));
                }
                if (validator.isValid(addUser)) {
                    User databaseUser = userService.getByLogin(addUser.getLogin());
                    if (addUser.getLogin().equals(databaseUser.getLogin())) {
                        req.setAttribute("message", "Login is already taken");
                    } else {
                        if (userService.add(addUser)) {
                            req.setAttribute("message", "User " + addUser.getLogin() + " added");
                        } else {
                            req.setAttribute("message", "User " + addUser.getLogin() + " not added");
                        }
                    }
                } else {
                    req.setAttribute("message", validator.fullnessCheck(addUser));
                }
                getServletContext().getRequestDispatcher("/WEB-INF/views/buffer.jsp").forward(req, resp);
                break;
            case "/web/userdel":
                addUser.setLogin(req.getParameter("login"));
                if (userService.delete(addUser)) {
                    req.setAttribute("message", "User " + addUser.getLogin() + " deleted");
                } else {
                    req.setAttribute("message", "User " + addUser.getLogin() + " is not found");
                }
                getServletContext().getRequestDispatcher("/WEB-INF/views/buffer.jsp").forward(req, resp);
                break;
            default:
                super.doPost(req, resp);
                break;
        }
    }

    private UserServiceFactory createUserServiceFactory(String applicationMode) {
        Map<String, UserServiceFactory> factories = Map.of(
                "DB", new DatabaseUserServiceFactory()
        );
        return factories.get(applicationMode);
    }
}