package ua.ithillel.dnipro.kremena.homework.utils.validator.impl;

import ua.ithillel.dnipro.kremena.homework.model.User;
import ua.ithillel.dnipro.kremena.homework.utils.validator.Validator;

import java.util.Objects;

public class UserValidator implements Validator<User> {

    @Override
    public boolean isValid(User user) {
        if (!Objects.isNull(user.getLogin()) && !user.getLogin().equalsIgnoreCase("")
                && !Objects.isNull(user.getPassword()) && !user.getPassword().equalsIgnoreCase("")
                && !Objects.isNull(user.getDateBorn()) && !user.getDateBorn().toString().equalsIgnoreCase("")) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String fullnessCheck(User user) {
        StringBuilder stringBuilder = new StringBuilder();
        if (Objects.isNull(user.getLogin()) || user.getLogin().equalsIgnoreCase("")) {
            stringBuilder.append("login ");
        }
        if (Objects.isNull(user.getPassword()) || user.getPassword().equalsIgnoreCase("")) {
            stringBuilder.append("password ");
        }
        if (Objects.isNull(user.getDateBorn()) || user.getDateBorn().toString().equalsIgnoreCase("")) {
            stringBuilder.append("date of birth ");
        }
        stringBuilder.append("not filled");
        return stringBuilder.toString();
    }
}