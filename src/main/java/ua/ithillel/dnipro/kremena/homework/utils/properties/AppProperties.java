package ua.ithillel.dnipro.kremena.homework.utils.properties;

import java.util.Optional;
import java.util.Properties;

public class AppProperties {

    public static final AppProperties INSTANCE = new AppProperties();

    private AppProperties() {}

    public Properties getProperties() {
        String applicationMode = System.getProperty("application.mode");
        String applicationView = System.getProperty("application.view");
        applicationMode = Optional.ofNullable(applicationMode).orElse("IN_MEMORY");
        applicationView = Optional.ofNullable(applicationView).orElse("CONSOLE");
        Properties properties = new Properties();
        properties.setProperty("application.mode", applicationMode);
        properties.setProperty("application.view", applicationView);
        try {
            switch (applicationMode) {
                case "DB" :
                    properties.load(
                            getClass().getClassLoader().getResourceAsStream("config/database_config.properties"));
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Storage settings missing " + e.getMessage());
        }
        return properties;
    }
}